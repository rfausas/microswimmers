from firedrake import *
import pygmsh
from pyop2.datatypes import IntType
import os
import csv
import numpy as np
from datetime import datetime
import sys
from scipy.optimize import root
import scipy.io
import copy
from geom_utils import *

parameters["form_compiler"]["quadrature_degree"] = 4

# Solver options
sp = {
    'snes_type': 'newtonls',
    'snes_monitor': None,
    'snes_converged_reason': None,
    'snes_linesearch_type': 'basic',
    'snes_max_it': 50,
    'snes_divergence_tolerance': 1e20,
    'snes_rtol': 1e-7,
    'snes_atol': 1e-10,
    'mat_type': 'aij',
    'ksp_type': 'preonly',
    'pc_type': 'lu',
    'pc_factor_mat_solver_type': 'mumps'
}

# IO
dir_   = './geom'
if not os.path.exists(dir_):
    os.mkdir(dir_)

# Tolerance: number near 1 from below
one_minus_eps = 1-1e-12

# Problem setting
L1 = 3.0 
L2 = 3.0
Y1left = 1
Y2left = 0.5*L2
S = 1.0
Lref = 1.0
thick = 0.03
refinement = Lref/100
gmsh_args= ["-algo", "front2d"]
   
# Rod's material parameters
E  = 1.e4
Ce = Constant(10.*E*thick**2) # Make it big -> penalize the extension
Ck = Constant(E*thick**3/12.)

tt = 0.0
scheme = 'semi_implicit' # can also be: explicit or pseudo_implicit

# Constant vector fields
noslip  = Constant((0, 0))

def viscosity(u):
    # Carreau fluid
    #po = 0.6
    #pwi = Constant((po-1)/2)
    #Cu2 = Constant(1)
    #gammadot2 = 2*inner(sym(grad(u)),sym(grad(u)))
    #ratiovis = Constant(0.5)
    #C1 = Constant(2.0)
    #return C1 + (ratiovis - C1)*(C1 + Cu2*gammadot2)**(pwi)
    return Constant(1.0)

#----------------------------------
# Finite element meshes and spaces

#----
# Rod
Nelrod  = 8
Nnodrod = Nelrod + 1
Lref  = 1.0
mesh_r= IntervalMesh(Nelrod,S)
Ndofs = 4*Nnodrod
h_r   = S / Nelrod
x_r   = SpatialCoordinate(mesh_r)
QH    = VectorFunctionSpace(mesh_r, 'HER', degree=3, dim=2)
eref  = project(as_vector([Constant(1), Constant(0)]), QH)
qH    = project(as_vector([x_r[0]*Lref/S + Y1left, Y2left]),QH) # Initial condition
qHn   = Function(QH)
qHdot = Function(QH)
res_fsi = np.zeros(Ndofs)
alpha   = np.zeros(Ndofs)
# Compute Rod nodal basis functions
count, deltaqH = 0, []
for i in range(Nnodrod):
    [deltaqH.append(Function(QH)) for d in range(4)]
    deltaqH[count+0].dat.data[2*i][0]   = 1
    deltaqH[count+1].dat.data[2*i][1]   = 1
    deltaqH[count+2].dat.data[2*i+1][0] = h_r
    deltaqH[count+3].dat.data[2*i+1][1] = h_r
    count = count + 4

#------
# Fluid
EM = MixedElement([VectorElement('Lagrange', 'triangle', 1), FiniteElement('Lagrange', 'triangle', 1)])
mesh_f, ids_bcs, W_f, x_f, list_nodes_wet, list_ord, nnod_f, nnod_wet, hk = \
MeshFluidRegion(L1, L2, Y1left, Y2left, Lref, thick, refinement, gmsh_args, EM, dir_)
sol_f  = Function(W_f)
v,q = TestFunctions(W_f)

# Important: Remember reference position of wet nodes
xref = np.zeros(shape=(nnod_wet,2))
for j in range(nnod_wet):
    k = list_nodes_wet[j]
    xref[j,:] = x_f[k,:]

#-------------------------------------------
# Auxiliary matrizes (UFL and numpy objects)
J, Jota = as_matrix([[0, -1], [1, 0]]), np.array([[0,-1],[1,0]])
I, iden = Identity(2), np.identity(2)

def epsilon(t,qp,L0):
    return (dot(t,qp) - L0)
def kappa(t,np,k0):    
    return (dot(t,np) - k0)

def depsilon(t,dq):
    return dot(t,dq.dx(0))
def dkappa(t,n,qp,qpp,dq):
    dnttmJ = 2*outer(n,t) - J
    dqp  = dq.dx(0)
    dqpp = dq.dx(0).dx(0)
    mqp2 = dot(qp,qp)
    aux  = dot(dnttmJ,dqp)
    return dot(qpp,aux)/mqp2 - dot(n,dqpp)/sqrt(mqp2)

def RodTerms(dt, eref, espont, fe, qHn, qHdot, dq):
    if(scheme == 'explicit' or scheme == 'semi_implicit'):
        qs = qHn
    elif(scheme == 'pseudo_implicit'):
        qs = qHn + Constant(dt)*qHdot
    qp  = qs.dx(0)
    qpp = qs.dx(0).dx(0)
    mqp = sqrt(dot(qp,qp))  
    t   = qp/mqp 
    n   = dot(J,t)
    Bper = I - outer(t,t)
    tp   = dot(Bper,qpp)/mqp
    np   = dot(J,tp)
    deps = depsilon(t,dq)
    eps  = epsilon(t,qp,eref[0]+espont[0])
    dkap = dkappa(t,n,qp,qpp,dq)
    kap  = kappa(t,np,eref[1]+espont[1])
    Fgrod = deps*Ce*eps + dkap*Ck*kap
    if(scheme == 'semi_implicit'): # Stabilization
        depsd = depsilon(t,qHdot)
        dkapd = dkappa(t,n,qp,qpp,qHdot)
        Fgrod += Constant(dt)*(depsd*Ce*deps + dkapd*Ck*dkap)
    Fgrod -= dot(fe[0],dq) + dot(fe[1],dq.dx(0))
    return assemble(Fgrod * dx(degree=5))

def ComputeRodQuants(qH,eref,espont):
   qp  = qH.dx(0)
   qpp = qH.dx(0).dx(0)
   mqp = sqrt(dot(qp,qp))  
   t = qp/mqp 
   n = dot(J,t)
   Bper = I - outer(t,t)
   tp = dot(Bper,qpp)/mqp
   np = dot(J,tp)
   ener = 0.5*assemble((Ce*epsilon(t,qp,eref[0]+espont[0])**2 + \
                        Ck*kappa(t,np,eref[1]+espont[1])**2)*dx(degree=5))
   mean_curv = assemble( kappa(t,np,eref[1]+espont[1])*dx(degree=5))
   return ener, mean_curv

def VelWet(xnodref,qp,qpp,Ndot,Ndotp):
    modqp = np.linalg.norm(qp)
    tan  = qp / modqp
    nn = np.dot(Jota,tan)
    nntensorqp =  np.tensordot(nn,qp,0)
    Bper = iden - np.tensordot(tan,tan,0)
    theta1,theta2 = ComputeThetas(xnodref)
    vel_wetnod = Ndot + theta1*Ndotp + \
                 theta2*aofs(modqp)/modqp*np.dot(Jota*Bper,Ndotp) + \
                 theta2*aofsp(qp,qpp)/modqp*np.dot(nntensorqp,Ndotp)
    return vel_wetnod

# Program these functions according to your problem
def SigmaofY(xnodref):
    sY = S*(xnodref[0] - Y1left)/Lref
    if(sY > S):
        sY = S
    return sY

def ComputeThetas(xnodref):
    sY = SigmaofY(xnodref)
    if(sY < S):
        theta1 = 0
    else:
        theta1 = xnodref[0] - Y1left - Lref
    theta2 = xnodref[1] - Y2left
    return theta1, theta2

def aofs(modqp):
    return 1/modqp
def aofsp(qp,qpp):
    return -np.dot(qp,qpp) / np.linalg.norm(qp)**3

# Hermite basis functions (Master element [-1,1])
def HermB(xi):
    return np.array([0.25*(2+xi)*(1-xi)**2, 0.125*(1+xi)*(1-xi)**2, \
                     0.25*(2-xi)*(1+xi)**2, -0.125*(1-xi)*(1+xi)**2])
def DHermB(xi):
      return np.array([0.75*(xi**2 - 1), 0.125*(-1 - 2*xi + 3*xi**2),\
                       0.75*(1 - xi**2),-0.125*(1 - 2*xi - 3*xi**2)])
def D2HermB(xi):
    return np.array([1.5*xi, 0.125*(-2 + 6*xi), -1.5*xi, 0.125*(2 + 6*xi)])

def EvalqH(qH, iel, xi):
   qx = np.array( [qH.vector()[2*iel+0][0], qH.vector()[2*iel+1][0],\
                   qH.vector()[2*iel+2][0], qH.vector()[2*iel+3][0]])
   qy = np.array( [qH.vector()[2*iel+0][1], qH.vector()[2*iel+1][1],\
                   qH.vector()[2*iel+2][1], qH.vector()[2*iel+3][1]] )
   Her = HermB(xi)
   return np.array([np.dot(Her,qx), np.dot(Her,qy)])

def EvalqHp(qH, iel, xi):
   qx = np.array( [qH.vector()[2*iel+0][0], qH.vector()[2*iel+1][0],\
                   qH.vector()[2*iel+2][0], qH.vector()[2*iel+3][0]] )
   qy = np.array( [qH.vector()[2*iel+0][1], qH.vector()[2*iel+1][1],\
                   qH.vector()[2*iel+2][1], qH.vector()[2*iel+3][1]] )
   DHer = (2/h_r)*DHermB(xi)
   return np.array([np.dot(DHer,qx), np.dot(DHer,qy)])

def EvalqHpp(qH, iel, xi):
   qx = np.array( [qH.vector()[2*iel+0][0], qH.vector()[2*iel+1][0],\
                   qH.vector()[2*iel+2][0], qH.vector()[2*iel+3][0]] )
   qy = np.array( [qH.vector()[2*iel+0][1], qH.vector()[2*iel+1][1],\
                   qH.vector()[2*iel+2][1], qH.vector()[2*iel+3][1]] )
   D2Her = (4/h_r**2)*D2HermB(xi)
   return np.array([np.dot(D2Her,qx), np.dot(D2Her,qy)])

def UpdateWetBoundary(xref, qH):
    print('|> Moving the wet boundary')
    new_y = np.zeros(shape=(nnod_wet,2))
    for j in range(nnod_wet): # Swept over wet nodes
        sY = SigmaofY(xref[j,:])
        el_inside = int(one_minus_eps * sY / h_r)
        xi = 2*sY/h_r - (2*el_inside+1)
        q = EvalqH(qH, el_inside, xi)
        qp = EvalqHp(qH, el_inside, xi)
        theta1,theta2 = ComputeThetas(xref[j,:])
        modqp = np.linalg.norm(qp)
        ny = q + theta1*qp + theta2*aofs(modqp)/modqp*np.dot(Jota,qp)
        new_y[j,:] = ny[:]
    return new_y

def SupportInfo(i, xnodref):
    xi = -10
    idnod = []
    sY = SigmaofY(xnodref)
    el_inside = int(one_minus_eps * sY / h_r)
    if(el_inside == i-1 or el_inside == i):
        if(el_inside == i-1): # right node: Herm(2) & Herm(3)
            xi = 2*sY / h_r - (2*i-1)
            idnod = [2,3]
        else: # left node: Herm(0) & Herm(1)
            xi = 2*sY / h_r - (2*i+1)
            idnod = [0,1]
    return el_inside, xi, idnod

def ApplyHhatqN(W_f, xref, list_nodes_wet, qH):
    print('|> Computing wh functions')
    scaling = np.array([1.,h_r])
    Ndot, Ndotp = np.zeros(2), np.zeros(2)
    count, wh = 0, []
    for i in range(Nnodrod): # Swept over rod nodes
        [wh.append(Function(W_f.sub(0), name='wh')) for d in range(4)]
        for j in range(nnod_wet): # Swept over wet fluid nodes
            k = list_nodes_wet[j]
            el_inside, xi, idn = SupportInfo(i, xref[j,:])
            if(el_inside == i-1 or el_inside == i):
                qp  = EvalqHp(qH, el_inside, xi)
                qpp = EvalqHpp(qH, el_inside, xi)
                HermBase, DerHermBase = HermB(xi), DHermB(xi)
                for d in range(2): # Swept over dofs per rod node
                    for c in range(2):
                        Ndot.fill(0.), Ndotp.fill(0.)
                        Ndot[c]  = scaling[d]*HermBase[idn[d]]
                        Ndotp[c] = scaling[d]*(2/h_r)*DerHermBase[idn[d]]
                        vel = VelWet(xref[j,:],qp,qpp,Ndot,Ndotp)
                        wh[count+c+2*d].dat.data[k][:] = vel[:]
        count = count + 4   
    print('|> wh functions done')
    return wh

def SolveFluid(u, p, v, q, hk, sol, bcs):
    mu = viscosity(u)
    tauh = hk*hk/(4*mu) # Stabilization parameter
    F  = ( inner(2*mu*sym(grad(u)), grad(v)) - div(v)*p )*dx
    F += ( q*div(u) + tauh*inner(grad(q), grad(p)) )*dx
    Jac = derivative(F, sol)
    solve(F == 0, sol, bcs=bcs, J=Jac, solver_parameters=sp)
    uo,p = split(sol)
    return uo,p

def residualFSI(alpha, mesh, hk, v, q, sol, wh, Ndofs, dt, eref, espont, fe, qHn, qHdot, deltaqH, bcs):
    print('\n')
    u,p = split(sol)
    for i in range(Ndofs):
        u += Constant(alpha[i])*wh[i]

    u,p = SolveFluid(u, p, v, q, hk, sol, bcs)
    for i in range(Ndofs):
        u += Constant(alpha[i])*wh[i]
    
    for i in range(Nnodrod):
        qHdot.dat.data[2*i][0]   = alpha[4*i+0]
        qHdot.dat.data[2*i][1]   = alpha[4*i+1]
        qHdot.dat.data[2*i+1][0] = alpha[4*i+2]*h_r
        qHdot.dat.data[2*i+1][1] = alpha[4*i+3]*h_r

    mu = viscosity(u)
    for i in range(Ndofs):            
        res_fsi[i]  = assemble( inner(2*mu*sym(grad(u)), grad(wh[i])) * dx )
        res_fsi[i] -= assemble( div(wh[i])*p * dx )
        res_fsi[i] += RodTerms(dt, eref, espont, fe, qHn, qHdot, deltaqH[i])

    print('\n')
    return res_fsi

case_bc_fluid = 1
def SetBCsFluid(tt, W_f, bc_case, ids_bcs):
    bcs = []
    if(bc_case == 1):
        bcs.append(DirichletBC(W_f.sub(0), noslip, ids_bcs[1]))
        bcs.append(DirichletBC(W_f.sub(0), noslip, ids_bcs[3]))
    elif(bc_case == 2):
        bcs.append(DirichletBC(W_f.sub(0), noslip, ids_bcs[0]))
        bcs.append(DirichletBC(W_f.sub(0), noslip, ids_bcs[2]))
        inlet = as_vector([0, 0.04*cos(1.5*pi*tt)*(xaux[0]**6)*(xaux[0]-L1)])
        bcs.append(DirichletBC(W_f.sub(0), inlet, ids_bcs[3]))
    bcs.append(DirichletBC(W_f.sub(0), noslip, ids_bcs[4]))
    return bcs

def SpontaneousStrain(tt, dt, x_r):
    if(scheme == 'explicit' or scheme == 'semi_implicit'):
        tav = tt
    elif(scheme == 'pseudo_implicit'):
        tav = tt + dt
    return as_vector ( [ Constant(0.0), 20*sin(4*pi*(x_r[0] + 2*tav)) ] )

def ExternalForce(tt, dt, x_r):
    return [project(as_vector([Constant(0), Constant(0)]), QH), project(as_vector([Constant(0), Constant(0)]), QH)]

file_evol = open('evol.txt', 'w')
dt = 1e-2
ntimes = 1000
tt = 0
for n in range(ntimes):

    print('|---------------------------------------------')
    print('|--- TIME STEP %d BEGINS - time= %lg' %(n, tt))
    print('|---------------------------------------------')
    
    startTime = datetime.now()

    qHn.assign(qH)

    # User defined data depending on time (bcs, eps0, kap0, etc)
    bcs_f  = SetBCsFluid(tt, W_f, case_bc_fluid, ids_bcs)
    espont = SpontaneousStrain(tt, dt, x_r)
    fe = ExternalForce(tt, dt, x_r)
    
    wh = ApplyHhatqN(W_f, xref, list_nodes_wet, qHn)
        
    solnl = root(residualFSI,
                 alpha,
                 args   = ((mesh_f, hk, v, q, sol_f, wh, Ndofs, dt,
                            eref, espont, fe, qHn, qHdot, deltaqH, bcs_f)),
                 method = 'krylov',
                 options= {'disp': True, 'fatol': 1e-6, 'maxiter': 9,
                           'line_search': 'armijo',
                           'jac_options': {'inner_tol': 1e-12, 'inner_maxiter': 100000, 'method': 'gmres', 'rdiff': 1e-4}})
    print(solnl.message)
    alpha = solnl.x
        
    u,p = split(sol_f)
    for i in range(Ndofs):
        u += Constant(alpha[i])*wh[i]
        
    SaveResults(n, u, p, viscosity(u), mesh_f, W_f, tt)

    fluid_dissip = assemble( (inner(2*viscosity(u)*sym(grad(u)), grad(u)) ) * dx )
    rod_energy, mean_curv = ComputeRodQuants(qH, eref, espont)
    file_evol.write("%0.10e %0.10e %0.10e %0.10e %0.10e\n"
                    %(tt, fluid_dissip, rod_energy, qH.vector()[0][0], qH.vector()[1][0]))
    file_evol.flush()

    qH.assign(qHn + Constant(dt)*qHdot)

    new_wet = UpdateWetBoundary(xref, qH)
    for i in range(nnod_wet): # Move the wet boundary
        x_f[list_nodes_wet[i],:] = new_wet[i,:]

    need_remesh = True
    if(need_remesh):
        mesh_f,ids_bcs,W_f,x_f,xref,list_nodes_wet,list_ord,nnod_f,nnod_wet,hk=\
        RemeshFluidRegion(mesh_f, x_f, xref, list_nodes_wet, list_ord, L1, L2, Lref, refinement, gmsh_args, EM, dir_)
        sol_f = Function(W_f)
        v,q = TestFunctions(W_f)
       
    tt = tt + dt
    
    time_delta = datetime.now() - startTime
    print('Elapse time: ', str(time_delta), '\n')

file_evol.close()

#------------
# End program
