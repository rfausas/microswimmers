# microswimmers

- Firedrake script for the simulation of active microswimmers as presented in:

  **"A finite element method for simulating soft active non-shearable rods immersed in generalized Newtonian fluids"**

   *by Roberto F. Ausas, Cristian G. Gebhardt and Gustavo C. Buscaglia*

   http://arxiv.org/abs/2107.12841

   Questions to: rfausas@icmc.usp.br  
                 http://www.lmacc.icmc.usp.br/~ausas/

- Firedrake installation:

  See detailed instructions in https://www.firedrakeproject.org/

  (The exact version of firedrake and its dependencies is written in the firedrake.json file 
  or better, see https://doi.org/10.5281/zenodo.5145878)

- Install gmsh 3.0.6 (older or newer versions might work as well)

- Activate the firedrake environment:

  . /home/user/path_to_your_firedrake_installation/firedrake/bin/activate

- Install pygmsh 6.1.0

  pip3 install pygmsh==6.1.0

  Assert file helpers.py: Go to the local python instalation directory 
  /home/user/path_to_your_firedrake_installation/firedrake/lib/pythonXX/
  and look for site-packages/pygmsh/helpers.py;
  In function generate_mesh search for "-bin" (~ at line 115) and comment it,
  such that the code will work with a .msh in text format.
  
- Running the script in a terminal:

  1. Download the script from: 

     https://gitlab.com/rfausas/microswimmers.git

  2. Activate the firedrake environment:

     . /home/user/path_to_your_firedrake_installation/firedrake/bin/activate

  3. Execute into your firedrake environment:

     (firedrake) user@machine:path_to_your_files$ python3 swimmer.py 


   Comment: Notice that the first time you run the script for a given rod discretization, the library expends
            some time during the code generation phase.


