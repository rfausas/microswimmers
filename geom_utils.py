from firedrake import *
from pyop2.datatypes import IntType
import numpy as np
import pygmsh

def GetFacetData(mesh_f):
    V = FunctionSpace(mesh_f, "CG", 1) 
    
    # this is adapted from firedrake/dmplex.pyx (boundary_nodes)
    facet_dim = mesh_f.facet_dimension()
    mesh_dim = mesh_f.geometric_dimension()
    
    boundary_dofs = V.finat_element.entity_support_dofs()[facet_dim]
    local_nodes = np.empty((len(boundary_dofs), 
                            len(boundary_dofs[0])),         
                           dtype=IntType)
    
    for k, v in boundary_dofs.items():
        local_nodes[k, :] = v
    
    facets = V.mesh().exterior_facets
    local_facets = facets.local_facet_dat.data_ro_with_halos

    nlocal = local_nodes.shape[1]  
    nfacet = facets.set.total_size

    facet_node_list = V.exterior_facet_node_map().values_with_halo

    # collect facet data and set up mesh object
    facet_data = np.zeros((nfacet, nlocal), dtype=IntType)
    for facet in range(nfacet):    
        l = local_nodes[local_facets[facet]]
        facet_data[facet,:] = facet_node_list[facet].take(l)

    return facet_data, nfacet

def GetWetConnectivity(mesh_f, W_f, ids_bcs):

    facet_data, nfacet = GetFacetData(mesh_f)
    
    # Extract wet boundary nodes
    list_nodes_wet = W_f.sub(0).boundary_nodes(ids_bcs[4], 'topological')
    nnod_wet = len(list_nodes_wet)
    print('hello', nnod_wet)

    # Put all nodes together
    new_list_nodes_wet = []
    for i in range(nnod_wet):
        new_list_nodes_wet.append(list_nodes_wet[i])

    list_nodes_wet_unique = np.unique(list_nodes_wet)
    nnod_wet = len(list_nodes_wet_unique)
    print('|> Initial nnod_wet= ', nnod_wet)

    lpoin = np.arange(nnod_wet+1)
    lpoin[:] = 0
    ip = list_nodes_wet_unique[0]
    lpoin[0] = ip
    markfacet =  np.arange(nfacet)
    markfacet[:] = 0

    for counter in range(nnod_wet):
        for facet in range(nfacet):
            if(markfacet[facet] == 0):
                n1 = facet_data[facet,0]
                n2 = facet_data[facet,1]
                if(n1 == lpoin[counter]):
                    lpoin[counter+1] = n2
                    markfacet[facet] = 1
                    break 
                elif(n2 == lpoin[counter]):
                    lpoin[counter+1] = n1
                    markfacet[facet] = 1
                    break

    return list_nodes_wet_unique, lpoin, nnod_wet, facet_data, nfacet

def GetWetConnectivityHalf(mesh_f, W_f, ids_bcs):

    facet_data, nfacet = GetFacetData(mesh_f)
    
    # Extract wet boundary nodes
    list_nodes_wet = W_f.sub(0).boundary_nodes(ids_bcs[4], 'topological')
    nnod_wet = len(list_nodes_wet)
    print('hello', nnod_wet)

    # Put all nodes together
    new_list_nodes_wet = []
    for i in range(nnod_wet):
        new_list_nodes_wet.append(list_nodes_wet[i])

    list_nodes_wet_unique = np.unique(list_nodes_wet)
    nnod_wet = len(list_nodes_wet_unique)
    print('|> Initial nnod_wet= ', nnod_wet)

    lpoin = np.arange(nnod_wet+1)
    lpoin[:] = 0

    leftmost_node = 0
    #target_point = np.array([0.0, 1.5-0.03/2])
    x_f = mesh_f.coordinates.dat.data
    for j in range(nnod_wet):
        k = list_nodes_wet_unique[j]
        aux_xnod = x_f[k,:]
        if(x_f[k,0] < 1e-10 and x_f[k,1] < 1.5):
            #dist = np.linalg.norm(aux_xnod - target_point)
            #if(dist < 1e-8):
            leftmost_node = j
            break
    print('|> Found left most node: ', leftmost_node, k)

    ip = list_nodes_wet_unique[leftmost_node]
    lpoin[0] = ip
    markfacet =  np.arange(nfacet)
    markfacet[:] = 0

    for counter in range(nnod_wet):
        for facet in range(nfacet):
            if(markfacet[facet] == 0):
                n1 = facet_data[facet,0]
                n2 = facet_data[facet,1]
                if((n1 in list_nodes_wet_unique) and (n2 in list_nodes_wet_unique)):
                    if(n1 == lpoin[counter]):
                        lpoin[counter+1] = n2
                        markfacet[facet] = 1
                        break 
                    elif(n2 == lpoin[counter]):
                        lpoin[counter+1] = n1
                        markfacet[facet] = 1
                        break

    return list_nodes_wet_unique, lpoin, nnod_wet, facet_data, nfacet

def SaveResults(n, ufin, pfin, mu, mesh_f, W_f, tt):
    
    #-----
    # Save the pressure
    myfile_pvd = open('newpressure.pvd', 'w')
    myfile_pvd.write('<?xml version="1.0" ?>\n')
    myfile_pvd.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n')
    myfile_pvd.write('<Collection>\n')
    for i in range(n+1):
         myfile_pvd.write('<DataSet timestep=\"'+str(i)+'\" file=\"pressure'+str(i)+'_0.vtu\" />\n')
    myfile_pvd.write('</Collection>\n')
    myfile_pvd.write('</VTKFile>\n')
    myfile_pvd.close()
    outfile_prem = File('pressure'+str(n)+'.pvd')
    outfile_prem.write(project(pfin,W_f.sub(1),name='pressure'), time=tt)
   
    #-----
    # Save the velocity
    myfile_pvd = open('newvelocity.pvd', 'w')
    myfile_pvd.write('<?xml version="1.0" ?>\n')
    myfile_pvd.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n')
    myfile_pvd.write('<Collection>\n')
    for i in range(n+1):
         myfile_pvd.write('<DataSet timestep=\"'+str(i)+'\" file=\"velocity'+str(i)+'_0.vtu\" />\n')
    myfile_pvd.write('</Collection>\n')
    myfile_pvd.write('</VTKFile>\n')
    myfile_pvd.close()
    outfile_urem = File('velocity'+str(n)+'.pvd')
    outfile_urem.write(project(ufin,W_f.sub(0),name='velocity'), time=tt)

    if(mu):
        #-----
        # Save the viscosity
        myfile_pvd = open('newviscosity.pvd', 'w')
        myfile_pvd.write('<?xml version="1.0" ?>\n')
        myfile_pvd.write('<VTKFile type="Collection" version="0.1" byte_order="LittleEndian">\n')
        myfile_pvd.write('<Collection>\n')
        for i in range(n+1):
            myfile_pvd.write('<DataSet timestep=\"'+str(i)+'\" file=\"viscosity'+str(i)+'_0.vtu\" />\n')
        myfile_pvd.write('</Collection>\n')
        myfile_pvd.write('</VTKFile>\n')
        myfile_pvd.close()
        outfile_visrem = File('viscosity'+str(n)+'.pvd')
        DG_vis = FunctionSpace(mesh_f, "DG", 0)
        outfile_visrem.write(project(mu, DG_vis,name='viscosity'), time=tt)

    return 0

def vphidummy(sigma):
   return np.array([sigma, 0])

def dervphidummy(sigma):
   return np.array([1, 0])

def thetathick(sigma,eta,thick):
   if(eta == -1):
      return -thick
   else:
      return thick

def MeshFluidRegionFlat(Lx, Ly, xini, yini, Lref, thick, refinement, gmsh_args, EM, dir_):
   # Characteristic length
   lcar = Lx/15.0
   nspoints = int(Lref/refinement)
   print(nspoints)
   lcir = Lref/(nspoints-1)
   lpot = lcar

   ids_bcs = []
   
   meshfile = dir_+"/beam"
   gmshbin = "/usr/bin/gmsh"

   # Create geometric object following gmsh format
   geometry = pygmsh.built_in.Geometry() #(gmsh_major_version=3)

   # Boundary box
   pb0 = geometry.add_point([0,0,0],lpot)
   pb1 = geometry.add_point([Lx,0,0],lpot)
   pb2 = geometry.add_point([Lx,Ly,0],lpot)
   pb3 = geometry.add_point([0,Ly,0],lpot)

   lb0 = geometry.add_line(pb0,pb1)
   lb1 = geometry.add_line(pb1,pb2)
   lb2 = geometry.add_line(pb2,pb3)
   lb3 = geometry.add_line(pb3,pb0)

   geometry.add_physical(lb0, label=0)
   ids_bcs.append(0)
   geometry.add_physical(lb1, label=1)
   ids_bcs.append(1)
   geometry.add_physical(lb2, label=2)
   ids_bcs.append(2)
   geometry.add_physical(lb3, label=3)
   ids_bcs.append(3)

   llbl = [lb0,lb1,lb2,lb3]
   llb = geometry.add_line_loop(llbl)

   # Beam points, lines, etc
   
   Ds = Lref/(nspoints-1)
   
   pb_beam = []
   
   Jota = np.array([[0,-1],[1,0]])
   aux = np.array([0,0])
   
   s = 0.0
   for n in range(nspoints):
      if(n == 0 or n == nspoints-1):
         lcirf = lcir/1.5
      else:
         lcirf = lcir
      vphidum = vphidummy(s)
      dvphidum = dervphidummy(s)
      eta = -1
      aux = vphidum + 0.5*thetathick(s,eta,thick)*np.dot(Jota,dvphidum) / np.linalg.norm(dvphidum)
      pb_aux = geometry.add_point([xini+aux[0], yini+aux[1], 0], lcirf)
      pb_beam.append(pb_aux)
      s = s + Ds
      
   s = 1.0
   for n in range(nspoints):
      if(n == 0 or n == nspoints-1):
         lcirf = lcir/1.5
      else:
         lcirf = lcir
      vphidum = vphidummy(s)
      dvphidum = dervphidummy(s)
      eta = 1
      aux = vphidum + 0.5*thetathick(s,eta,thick)*np.dot(Jota,dvphidum) / np.linalg.norm(dvphidum)
      pb_aux = geometry.add_point([xini+aux[0], yini+aux[1], 0], lcirf)
      pb_beam.append(pb_aux)
      s = s - Ds

   ll_beam = []
   for n in range(2*nspoints-1):
       lb_aux = geometry.add_line(pb_beam[n],pb_beam[n+1])
       ll_beam.append(lb_aux)

   lb_aux = geometry.add_line(pb_beam[2*nspoints-1],pb_beam[0])
   ll_beam.append(lb_aux)
   ll_beam_id = geometry.add_line_loop(ll_beam)

   id_beam = 4
   geometry.add_physical(ll_beam, label=id_beam)
   ids_bcs.append(id_beam)

   fsurf = geometry.add_plane_surface(llb, holes=[ll_beam_id])
   geometry.add_physical(fsurf, label=5)

   mesh = pygmsh.generate_mesh(geometry, dim=2, gmsh_path=gmshbin, extra_gmsh_arguments=gmsh_args,
                               msh_filename=meshfile+".msh", geo_filename=meshfile+".geo")

   mesh_f = Mesh(meshfile+'.msh')
   x_f    = mesh_f.coordinates.dat.data
   hk     = CellDiameter(mesh_f)
   nnod_f = len(x_f[:,0])
   W_f    = FunctionSpace(mesh_f, EM)
   list_nodes_wet, lpoin, nnod_wet, facet_data, nfacet = GetWetConnectivity(mesh_f, W_f, ids_bcs)
   return mesh_f, ids_bcs, W_f, x_f, list_nodes_wet, lpoin, nnod_f, nnod_wet, hk

def MeshFluidRegion(Lx, Ly, xini, yini, Lref, thick, refinement, gmsh_args, EM, dir_):
   # Characteristic length
   lcar = Lx/15.0
   nspoints = int(Lref/refinement)
   lcir = Lref/(nspoints-1)
   lpot = lcar
   eof = thick/2

   ids_bcs = []
   
   meshfile = dir_+"/beam"
   gmshbin = "/usr/bin/gmsh"

   # Create geometric object following gmsh format
   geometry = pygmsh.built_in.Geometry() #(gmsh_major_version=3)

   # Boundary box
   pb0 = geometry.add_point([0,0,0],lpot)
   pb1 = geometry.add_point([Lx,0,0],lpot)
   pb2 = geometry.add_point([Lx,Ly,0],lpot)
   pb3 = geometry.add_point([0,Ly,0],lpot)

   lb0 = geometry.add_line(pb0,pb1)
   lb1 = geometry.add_line(pb1,pb2)
   lb2 = geometry.add_line(pb2,pb3)
   lb3 = geometry.add_line(pb3,pb0)

   geometry.add_physical(lb0, label=0)
   ids_bcs.append(0)
   geometry.add_physical(lb1, label=1)
   ids_bcs.append(1)
   geometry.add_physical(lb2, label=2)
   ids_bcs.append(2)
   geometry.add_physical(lb3, label=3)
   ids_bcs.append(3)

   llbl = [lb0,lb1,lb2,lb3]
   llb = geometry.add_line_loop(llbl)

   # Beam points, lines, etc
   
   Ds = Lref/(nspoints-1)
   
   pb_beam = []
   
   Jota = np.array([[0,-1],[1,0]])
   aux = np.array([0,0])

   Ds = (Lref-eof)/(nspoints-1)
   
   s = eof
   for n in range(nspoints):
      lcirf = lcir
      vphidum = vphidummy(s)
      dvphidum = dervphidummy(s)
      eta = -1
      aux = vphidum + 0.5*thetathick(s,eta,thick)*np.dot(Jota,dvphidum) / np.linalg.norm(dvphidum)
      pb_aux = geometry.add_point([xini+aux[0], yini+aux[1], 0], lcirf)
      pb_beam.append(pb_aux)
      s = s + Ds
      
   s = 1.0
   for n in range(nspoints):
      lcirf = lcir
      vphidum = vphidummy(s)
      dvphidum = dervphidummy(s)
      eta = 1
      aux = vphidum + 0.5*thetathick(s,eta,thick)*np.dot(Jota,dvphidum) / np.linalg.norm(dvphidum)
      pb_aux = geometry.add_point([xini+aux[0], yini+aux[1], 0], lcirf)
      pb_beam.append(pb_aux)
      s = s - Ds
   
   ll_beam = []
   
   for n in range(nspoints-1):
       lb_aux = geometry.add_line(pb_beam[n],pb_beam[n+1])
       ll_beam.append(lb_aux)

   beta = 3
   hd = beta*thick
   hp = 0.5*hd - sqrt((0.5*hd)**2 - eof**2)
   hl = hd

   p1r = geometry.add_point([xini+Lref -hp +0.5*hd, yini+0.5*hd, 0], lcirf)
   p2r = geometry.add_point([xini+Lref -hp +0.5*hd, yini-0.5*hd, 0], lcirf)
   p3r = geometry.add_point([xini+Lref -hp +0.5*hd, yini       , 0], lcirf)
   
   p4r = geometry.add_point([xini+Lref -hp +0.5*hd +hl, yini-0.5*hd, 0], lcirf)
   p5r = geometry.add_point([xini+Lref -hp +0.5*hd +hl, yini       , 0], lcirf)
   p6r = geometry.add_point([xini+Lref -hp +0.5*hd +hl, yini+0.5*hd, 0], lcirf)
   p7r = geometry.add_point([xini+Lref -hp +    hd +hl, yini       , 0], lcirf)

   c0r = geometry.add_circle_arc(pb_beam[nspoints-1], p3r, p2r)
   ll_beam.append(c0r)

   lb_aux = geometry.add_line(p2r,p4r)
   ll_beam.append(lb_aux)

   c1r = geometry.add_circle_arc(p4r, p5r, p7r)
   ll_beam.append(c1r)

   c2r = geometry.add_circle_arc(p7r, p5r, p6r)
   ll_beam.append(c2r)

   lb_aux = geometry.add_line(p6r,p1r)
   ll_beam.append(lb_aux)
   
   c3r = geometry.add_circle_arc(p1r, p3r, pb_beam[nspoints])
   ll_beam.append(c3r)

   for n in range(nspoints,2*nspoints-1):
      lb_aux = geometry.add_line(pb_beam[n],pb_beam[n+1])
      ll_beam.append(lb_aux)
   
   p1 = geometry.add_point([xini+eof, yini, 0], lcirf)
   p2 = geometry.add_point([xini+eof, yini+eof, 0], lcirf)
   p3 = geometry.add_point([xini, yini, 0], lcirf)
   p4 = geometry.add_point([xini+eof, yini-eof, 0], lcirf)

   c0 = geometry.add_circle_arc(pb_beam[0], p1, p3)
   c1 = geometry.add_circle_arc(p3, p1, pb_beam[2*nspoints-1])

   ll_beam.append(-c1)
   ll_beam.append(-c0)
   
   ll_beam_id = geometry.add_line_loop(ll_beam)

   id_beam = 4
   geometry.add_physical(ll_beam, label=id_beam)
   ids_bcs.append(id_beam)

   fsurf = geometry.add_plane_surface(llb, holes=[ll_beam_id])
   geometry.add_physical(fsurf, label=5)

   mesh = pygmsh.generate_mesh(geometry, dim=2, gmsh_path=gmshbin, extra_gmsh_arguments=gmsh_args,
                               msh_filename=meshfile+".msh", geo_filename=meshfile+".geo")
   
   mesh_f = Mesh(meshfile+'.msh')
   x_f    = mesh_f.coordinates.dat.data
   hk     = CellDiameter(mesh_f)
   nnod_f = len(x_f[:,0])
   W_f    = FunctionSpace(mesh_f, EM)
   list_nodes_wet, lpoin, nnod_wet, facet_data, nfacet = GetWetConnectivity(mesh_f, W_f, ids_bcs)
   return mesh_f, ids_bcs, W_f, x_f, list_nodes_wet, lpoin, nnod_f, nnod_wet, hk

def RemeshFluidRegion(mesh_p, x_f, xref, list_nodes_wet, lpoin, Lx, Ly, Lref, refinement, gmsh_args, EM, dir_):
   # Characteristic length
   lcar = Lx/15.0
   nspoints = int(Lref/refinement)
   lcir = 4*Lref/(nspoints-1) # to ensure no new points are created in between
   lpot = lcar

   ids_bcs = []
   
   meshfile = dir_+"/beam"
   gmshbin = "/usr/bin/gmsh"

   # Create geometric object following gmsh format
   geometry = pygmsh.built_in.Geometry() #(gmsh_major_version=3)

   # Boundary box
   pb0 = geometry.add_point([0,0,0],lpot)
   pb1 = geometry.add_point([Lx,0,0],lpot)
   pb2 = geometry.add_point([Lx,Ly,0],lpot)
   pb3 = geometry.add_point([0,Ly,0],lpot)

   lb0 = geometry.add_line(pb0,pb1)
   lb1 = geometry.add_line(pb1,pb2)
   lb2 = geometry.add_line(pb2,pb3)
   lb3 = geometry.add_line(pb3,pb0)

   geometry.add_physical(lb0, label=0)
   ids_bcs.append(0)
   geometry.add_physical(lb1, label=1)
   ids_bcs.append(1)
   geometry.add_physical(lb2, label=2)
   ids_bcs.append(2)
   geometry.add_physical(lb3, label=3)
   ids_bcs.append(3)

   llbl = [lb0,lb1,lb2,lb3]
   llb = geometry.add_line_loop(llbl)

   # Beam points, lines, etc
   pb_beam = []
   for n in range(len(lpoin)-1):
      k = lpoin[n]
      xnod = x_f[k,0]
      ynod = x_f[k,1]
      pb_aux = geometry.add_point([xnod, ynod, 0], lcir)   
      pb_beam.append(pb_aux)

   ll_beam = []
   for n in range(len(lpoin)-2):
       lb_aux = geometry.add_line(pb_beam[n],pb_beam[n+1])
       ll_beam.append(lb_aux)

   lb_aux = geometry.add_line(pb_beam[len(lpoin)-2],pb_beam[0])
   ll_beam.append(lb_aux)

   ll_beam_id = geometry.add_line_loop(ll_beam)

   id_beam = 4

   geometry.add_physical(ll_beam, label=id_beam)
   ids_bcs.append(id_beam)

   fsurf = geometry.add_plane_surface(llb, holes=[ll_beam_id])

   geometry.add_physical(fsurf)

   mesh = pygmsh.generate_mesh(geometry, dim=2, gmsh_path=gmshbin, extra_gmsh_arguments=gmsh_args,
                               msh_filename=meshfile+".msh", geo_filename=meshfile+".geo")

   new_mesh_f = Mesh(meshfile+'.msh')

   new_W_f = FunctionSpace(new_mesh_f, EM)
   new_list_nodes_wet, new_lpoin, nnod_wet, facet_data, nfacet = GetWetConnectivity(new_mesh_f, new_W_f, ids_bcs)
   if(len(new_list_nodes_wet) != len(list_nodes_wet)):
       raise Exception('incompatible rod boundary lists')

   new_xref = np.zeros(shape=(len(new_list_nodes_wet),2))
   new_x_f = new_mesh_f.coordinates.dat.data
   for j in range(len(new_list_nodes_wet)):
       k = new_list_nodes_wet[j]
       aux_xnod = new_x_f[k,:]
       for j2 in range(len(list_nodes_wet)):
           k2 = list_nodes_wet[j2]
           aux_xnod_prev = x_f[k2,:]
           dist = np.linalg.norm(aux_xnod - aux_xnod_prev)
           if(dist < 1e-8):
               new_xref[j,:] = xref[j2,:]
               break
           
   # Finally ...
   mesh_f = new_mesh_f
   xref = new_xref
   x_f  = new_x_f
   hk   = CellDiameter(mesh_f)
   W_f  = new_W_f
   list_nodes_wet = new_list_nodes_wet
   nnod_f = len(x_f[:,0])
   nnod_wet = len(list_nodes_wet)
   print('|> After remeshing nnod_wet= ', nnod_wet)
   return mesh_f, ids_bcs, W_f, x_f, xref, list_nodes_wet, new_lpoin, nnod_f, nnod_wet, hk

def MeshFluidRegionHalf(Lx, Ly, xini, yini, Lref, thick, refinement, gmsh_args, EM, dir_):
   # Characteristic length
   lcar = Lx/20.0
   nspoints = int(Lref/refinement)
   lcir = Lref/(nspoints-1)
   lpot = lcar
   eof = thick/2

   ids_bcs = []
   
   meshfile = dir_+"/beam"
   gmshbin = "/usr/bin/gmsh"

   # Create geometric object following gmsh format
   geometry = pygmsh.built_in.Geometry() #(gmsh_major_version=3)

   # Boundary box
   pb0 = geometry.add_point([0,0,0],lpot)
   pb1 = geometry.add_point([Lx,0,0],lpot)
   pb2 = geometry.add_point([Lx,Ly,0],lpot)
   pb3 = geometry.add_point([0,Ly,0],lpot)

   lb0 = geometry.add_line(pb0,pb1)
   lb1 = geometry.add_line(pb1,pb2)
   lb2 = geometry.add_line(pb2,pb3)
   
   geometry.add_physical(lb0, label=0)
   ids_bcs.append(0)
   geometry.add_physical(lb1, label=1)
   ids_bcs.append(1)
   geometry.add_physical(lb2, label=2)
   ids_bcs.append(2)

   ids_bcs.append(3) # Dummy: for compatibility with other cases

   # Beam points, lines, etc
   
   pb_beam = []
   
   Jota = np.array([[0,-1],[1,0]])
   aux = np.array([0,0])

   Ds = (Lref-eof)/(nspoints-1)
   
   s = 0
   for n in range(nspoints):
      lcirf = lcir
      vphidum = vphidummy(s)
      dvphidum = dervphidummy(s)
      eta = -1
      aux = vphidum + 0.5*thetathick(s,eta,thick)*np.dot(Jota,dvphidum) / np.linalg.norm(dvphidum)
      pb_aux = geometry.add_point([xini+aux[0], yini+aux[1], 0], lcirf)
      pb_beam.append(pb_aux)
      s = s + Ds
      
   s = 1.0-eof
   for n in range(nspoints):
      lcirf = lcir
      vphidum = vphidummy(s)
      dvphidum = dervphidummy(s)
      eta = 1
      aux = vphidum + 0.5*thetathick(s,eta,thick)*np.dot(Jota,dvphidum) / np.linalg.norm(dvphidum)
      pb_aux = geometry.add_point([xini+aux[0], yini+aux[1], 0], lcirf)
      pb_beam.append(pb_aux)
      s = s - Ds
   
   ll_beam = []   
   for n in range(nspoints-1):
       lb_aux = geometry.add_line(pb_beam[n],pb_beam[n+1])
       ll_beam.append(lb_aux)

   p1r = geometry.add_point([xini+Lref-eof, yini, 0], lcirf)
   p2r = geometry.add_point([xini+Lref-eof, yini+eof, 0], lcirf)
   p3r = geometry.add_point([xini+Lref, yini, 0], lcirf)
   p4r = geometry.add_point([xini+Lref-eof, yini-eof, 0], lcirf)

   c0r = geometry.add_circle_arc(pb_beam[nspoints-1], p1r, p3r)
   c1r = geometry.add_circle_arc(p3r, p1r, pb_beam[nspoints])

   ll_beam.append(c0r)
   ll_beam.append(c1r)

   for n in range(nspoints,2*nspoints-1):
      lb_aux = geometry.add_line(pb_beam[n],pb_beam[n+1])
      ll_beam.append(lb_aux)
   
   ll_beam_id = geometry.add_line_loop(ll_beam)

   id_beam = 4
   geometry.add_physical(ll_beam, label=id_beam)
   ids_bcs.append(id_beam)

   lb3 = geometry.add_line(pb3,pb_beam[2*nspoints-1])
   lb4 = geometry.add_line(pb_beam[0],pb0)

   geometry.add_physical(lb3, label=5)
   ids_bcs.append(5)
   geometry.add_physical(lb4, label=6)
   ids_bcs.append(6)

   llbl = [lb0,lb1,lb2,lb3]
   for i in range(len(ll_beam)-1,-1,-1):
      llbl.append(-ll_beam[i])
   llbl.append(lb4)
   llb = geometry.add_line_loop(llbl)
   
   fsurf = geometry.add_plane_surface(llb)
   geometry.add_physical(fsurf)

   mesh = pygmsh.generate_mesh(geometry, dim=2, gmsh_path=gmshbin, extra_gmsh_arguments=gmsh_args,
                               msh_filename=meshfile+".msh", geo_filename=meshfile+".geo")

   mesh_f = Mesh(meshfile+'.msh')
   x_f    = mesh_f.coordinates.dat.data
   hk     = CellDiameter(mesh_f)
   nnod_f = len(x_f[:,0])
   W_f    = FunctionSpace(mesh_f, EM)
   list_nodes_wet, lpoin, nnod_wet, facet_data, nfacet = GetWetConnectivityHalf(mesh_f, W_f, ids_bcs)
   return mesh_f, ids_bcs, W_f, x_f, list_nodes_wet, lpoin, nnod_f, nnod_wet, hk

def RemeshFluidRegionHalf(mesh_p, x_f, xref, list_nodes_wet, lpoin, Lx, Ly, Lref, refinement, gmsh_args, EM, dir_):
   # Characteristic length
   lcar = Lx/20.0
   nspoints = int(Lref/refinement)
   lcir = 4*Lref/(nspoints-1)
   lpot = lcar

   ids_bcs = []
   
   meshfile = dir_+"/beam"
   gmshbin = "/usr/bin/gmsh"

   # Create geometric object following gmsh format
   geometry = pygmsh.built_in.Geometry() #(gmsh_major_version=3)

   # Boundary box
   pb0 = geometry.add_point([0,0,0],lpot)
   pb1 = geometry.add_point([Lx,0,0],lpot)
   pb2 = geometry.add_point([Lx,Ly,0],lpot)
   pb3 = geometry.add_point([0,Ly,0],lpot)

   lb0 = geometry.add_line(pb0,pb1)
   lb1 = geometry.add_line(pb1,pb2)
   lb2 = geometry.add_line(pb2,pb3)

   geometry.add_physical(lb0, label=0)
   ids_bcs.append(0)
   geometry.add_physical(lb1, label=1)
   ids_bcs.append(1)
   geometry.add_physical(lb2, label=2)
   ids_bcs.append(2)

   ids_bcs.append(3) # Dummy: for compatibility with other cases

   # Beam points, lines, etc
   pb_beam = []
   for n in range(len(lpoin)-1):
      k = lpoin[n]
      xnod = x_f[k,0]
      ynod = x_f[k,1]
      #print(n, k, xnod, ynod)
      if(n == 0 or n == (len(lpoin)-1)):
         lcirf = lcir/2
      else:
         lcirf = lcir
      pb_aux = geometry.add_point([xnod, ynod, 0], lcirf)   
      pb_beam.append(pb_aux)
   
   ll_beam = []
   for n in range(len(lpoin)-2,0,-1):
       lb_aux = geometry.add_line(pb_beam[n],pb_beam[n-1])
       ll_beam.append(lb_aux)

   ll_beam_id = geometry.add_line_loop(ll_beam)

   id_beam = 4
   
   geometry.add_physical(ll_beam, label=id_beam)
   ids_bcs.append(id_beam)

   lb3 = geometry.add_line(pb3,pb_beam[len(lpoin)-2])
   geometry.add_physical(lb3, label=5)
   ids_bcs.append(5)

   lb4 = geometry.add_line(pb_beam[0],pb0)
   geometry.add_physical(lb4, label=6)
   ids_bcs.append(6)

   llbl = [lb0,lb1,lb2,lb3]
   for i in range(len(ll_beam)):
      llbl.append(ll_beam[i])
   llbl.append(lb4)
   llb = geometry.add_line_loop(llbl)

   fsurf = geometry.add_plane_surface(llb)

   geometry.add_physical(fsurf)

   mesh = pygmsh.generate_mesh(geometry, dim=2, gmsh_path=gmshbin, extra_gmsh_arguments=gmsh_args,
                               msh_filename=meshfile+".msh", geo_filename=meshfile+".geo")

   new_mesh_f = Mesh(meshfile+'.msh')

   new_W_f = FunctionSpace(new_mesh_f, EM)
   new_list_nodes_wet, new_lpoin, nnod_wet, facet_data, nfacet = GetWetConnectivityHalf(new_mesh_f, new_W_f, ids_bcs)
   if(len(new_list_nodes_wet) != len(list_nodes_wet)):
       raise Exception('incompatible rod boundary lists')

   new_xref = np.zeros(shape=(len(new_list_nodes_wet),2))
   new_x_f = new_mesh_f.coordinates.dat.data
   
   for j in range(len(new_list_nodes_wet)):
       k = new_list_nodes_wet[j]
       aux_xnod = new_x_f[k,:]
       for j2 in range(len(list_nodes_wet)):
           k2 = list_nodes_wet[j2]
           aux_xnod_prev = x_f[k2,:]
           dist = np.linalg.norm(aux_xnod - aux_xnod_prev)
           if(dist < 1e-8):
               new_xref[j,:] = xref[j2,:]
               break
           
   # Finally ...
   mesh_f = new_mesh_f
   xref = new_xref
   x_f  = new_x_f
   hk   = CellDiameter(mesh_f)
   W_f  = new_W_f
   list_nodes_wet = new_list_nodes_wet
   nnod_f = len(x_f[:,0])
   nnod_wet = len(list_nodes_wet)
   print('|> After remeshing nnod_wet= ', nnod_wet)
   return mesh_f, ids_bcs, W_f, x_f, xref, list_nodes_wet, new_lpoin, nnod_f, nnod_wet, hk
